const requiredInRange = (str, min, max, name) => {
    if (str === undefined) return `${name} is required`;
    if (typeof str !== 'string') return `${name} must be a string`;
    if (str.length < min) return `${name} must be in range [${min}..${max}]`;
    if (str.length > max) return `${name} must be in range [${min}..${max}]`;
    return null;
};

const optionalInRange = (str, min, max, name) => {
    if (str === undefined) return null;
    if (typeof str !== 'string') return `${name} must be a string`;
    if (str.length < min) return `${name} must be in range [${min}..${max}]`;
    if (str.length > max) return `${name} must be in range [${min}..${max}]`;
    return null;
};

const numberInRange = (num, min, max, name) => {
    if (num === undefined) return `${name} is required`;
    if (typeof num !== 'number') return `${name} must be a number`;
    if (num < min) return `${name} must be in range [${min}..${max}]`;
    if (num > max) return `${name} must be in range [${min}..${max}]`;
    return null;
};

const arrayWithMinLength = (arr, min, name) => {
    if (arr === undefined) return `${name} is required`;
    if (!Array.isArray(arr)) return `${name} must be an array`;
    if (arr.length < min) return `${name} must contain at least ${min} elements`;
    return null;
};

const possibleValues = (value, name, options) => {
    if (value === undefined) return `${name} is required`;
    if (!options.includes(value)) return `${name} must be one of [${options.join(', ')}]`
    return null;
};

export default {
    login: {
        username: (s) => requiredInRange(s, 3, 30, 'username'),
        password: (s) => requiredInRange(s, 3, 30, 'password'),
    },
    register: {
        username: (s) => requiredInRange(s, 3, 30, 'username'),
        password: (s) => requiredInRange(s, 3, 30, 'password'),
    },
    updateUser: {
        displayName: (s) => optionalInRange(s, 3, 30, 'displayName')
    },
    createBar: {
        name: (s) => requiredInRange(s, 5, 50, 'name'),
        address: (s) => requiredInRange(s, 5, 100, 'address'),
        phone: (s) => requiredInRange(s, 2, 20, 'phone'),
    },
    updateBar: {
        name: (s) => optionalInRange(s, 5, 50, 'name'),
        address: (s) => optionalInRange(s, 5, 100, 'address'),
        phone: (s) => optionalInRange(s, 2, 20, 'phone'),
    },
    createReview: {
        text: (s) => requiredInRange(s, 25, 1000, 'text'),
        rating: (n) => numberInRange(n, 1, 5, 'rating')
    },
    ingredient: {
        name: (s) => requiredInRange(s, 2, 50, 'name')
    },
    createCocktail: {
        name: (s) => requiredInRange(s, 2, 50, 'name'),
        ingredients: (a) => arrayWithMinLength(a, 1, 'ingredients')
    },
    updateCocktail: {
        name: (s) => optionalInRange(s, 2, 50, 'name'),
    },
    addIngredients: {
        ingredients: (a) => arrayWithMinLength(a, 1, 'ingredients')
    },
    removeIngredients: {
        ingredients: (a) => arrayWithMinLength(a, 1, 'ingredients')
    },
    hideImageActions: {
        action: (s) => possibleValues(s, 'action', ['hide', 'show'])
    }
};