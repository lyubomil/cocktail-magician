import { createContext } from 'react';

const BarContext = createContext(null);

export default BarContext;
