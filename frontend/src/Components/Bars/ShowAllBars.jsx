import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { getBars, getBarsByName, getBarsByIngredient } from '../../requests/private-requests';
import { useLocation } from 'react-router-dom';
import Form from 'react-bootstrap/Form';

// eslint-disable-next-line react/prop-types
const ShowAllBars = ({ history }) =>{
  const [bars, setBars] = useState([]);
  const [form, setForm] = useState({});

  useEffect(() => {
    getBars()
      .then(data => {
        setBars(data);
      });
  }, []);

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const searchBarByNameHandler = (e) => {
    e.preventDefault();
    // eslint-disable-next-line react/prop-types
    history.push({
      search: `?name=${form.barName ? form.barName : ''}`
    });
    getBarsByName(form.barName).then(res => setBars(res));
  };

  const searchBarByIngredientHandler = (e) => {
    e.preventDefault();
    // eslint-disable-next-line react/prop-types
    history.push({
      search: `?name=${form.ingredientName ? form.ingredientName : ''}`
    });
    getBarsByIngredient(form.ingredientName).then(res => {
      setBars(res[0].bars);
    });
  };

  const routeChange = (id) =>{
    // eslint-disable-next-line react/prop-types
    history.push(`/bars/${id}`);
  };

  return (
    <Container>
      <Form className="search-form" method='GET' noValidate autoComplete="off">
        <Form.Group className="mb-4" controlId="formBasicText">
          <Form.Control type="text" placeholder="Bar name" required onChange={e => setField('barName', e.target.value)} />
        </Form.Group>
        <Button variant="primary" type="submit" onClick={searchBarByNameHandler}>
          Search
        </Button>
      </Form>
      <Form className="search-form" method='GET' noValidate autoComplete="off">
        <Form.Group className="mb-4" controlId="formBasicText">
          <Form.Control type="text" placeholder="Ingredient" required onChange={e => setField('ingredientName', e.target.value)} />
        </Form.Group>
        <Button variant="primary" type="submit" onClick={searchBarByIngredientHandler}>
          Search
        </Button>
      </Form>
      <Row xs={1} md={3} className="g-4">
        {bars.map(bar =>{
          return (
            <Col key={bar.id ? bar.id : bar.barId}>
              <Container>
                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src={typeof(bar.images) === 'undefined' ? '' : `http://localhost:5000${bar.images[0].url}`} />
                  <Card.Body>
                    <Card.Title>{bar.name ? bar.name : bar.barName}</Card.Title>
                    <Card.Text>Phone: {bar.phone}</Card.Text>
                    <Card.Text>Address: {bar.address}</Card.Text>
                    {bar.barId ?
                      <div>
                        <Card.Text>Cocktail Name: {bar.cocktailName}</Card.Text>
                        <Card.Img variant="top" src={typeof(bar.cocktailImage) === 'undefined' ? '' : `http://localhost:5000${bar.cocktailImage}`} />
                      </div> :
                      <div> </div>}
                    <Button onClick={() => routeChange((bar.id ? bar.id : bar.barId))}>
                      Details
                    </Button>
                  </Card.Body>
                </Card>
              </Container>
            </Col>
          );
        })}
      </Row>
    </Container>
  );

};

ShowAllBars.propTypes = {
  history: PropTypes.object,
};


export default ShowAllBars;
