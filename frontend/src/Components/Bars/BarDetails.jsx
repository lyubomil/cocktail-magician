import React from 'react';
import Container from 'react-bootstrap/Container';
import { useState, useEffect, useContext } from 'react';
import { fetchBarRequest } from '../../requests/server-requests';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { AuthContext } from '../../context/auth-context';

// eslint-disable-next-line react/prop-types
const BarDetails = ({ match, history }) => {
  const [bar, setBar] = useState([]);
  const { user, isLoggedIn } = useContext(AuthContext);

  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    fetchBarRequest(`bars/${match.params.id}`)
      .then(barResult => {
        setBar(barResult);
        console.log(user);
      })
      .catch(e => {
        console.log(e);
      });
  }, []);

  const adminHandler = () => {
    if (user?.role === 'magician') { // TODO Check for admin permissions
      return (
        <div>
          {/* eslint-disable-next-line react/prop-types */}
          <Button onClick={() => history.push(`/bars/edit/${bar.id}`)}>
            Edit bar
          </Button>
          {/* eslint-disable-next-line react/prop-types */}
          <Button onClick={() => history.push(`/bars/editCocktails/${bar.id}`)}>
            Edit cocktail colletion
          </Button>
        </div>
      );
    } else {
      return '';
    };
  };

  const getCocktails = (e) => {
    const cocktails = bar.cocktails;
    if (cocktails?.length > 0) {
      return (
        cocktails.map(c => {
          console.log(c);
          return (
            <div key={c.id}>
              <Card style={{ width: '25%', float: 'left' }}>
                <Card.Img variant="top" src={`http://localhost:5000${c.imageUrl}`} />
                <Card.Body>
                  <Card.Title>{c.name}</Card.Title>
                  <Card.Body>
                    Ingredients: {c.ingredients}
                  </Card.Body>
                </Card.Body>
              </Card>
            </div>
          );
        })
      );
    } else {
      return '';
    };
  };

  return (
    <Container className="centeredCont">
      <Card style={{ width: '50%' }}>
        <Card.Img variant="top" src={typeof (bar.images) === 'undefined' ? 'f' : `http://localhost:5000${bar.images[0].url}`} />
        <Card.Body>
          <Card.Title>{bar.name}</Card.Title>
          <Card.Body>
            Address: {bar.address}
            <br />
            Phone: {bar.phone}
            <br />
            Average rating: {bar.rating ? bar.rating : 'Not reviewed yet!'}
          </Card.Body>
        </Card.Body>
        {adminHandler()}
        {user && user?.role !== '' ?
          /* eslint-disable-next-line react/prop-types */
          <Button onClick={() => history.push(`/bars/add-review/${bar.id}`)}>
            Write review
          </Button> :
          <div> </div>}
        {bar.reviewsCount > 0 ?
        /* eslint-disable-next-line react/prop-types */
          <Button onClick={() => history.push(`/bars/reviews/${bar.id}`)}>
            See reviews
          </Button> :
          <div>  </div>}
      </Card>
      {getCocktails()}
    </Container>
  );
};

export default BarDetails;
