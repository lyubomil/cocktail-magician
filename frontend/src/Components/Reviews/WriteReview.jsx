/* eslint linebreak-style: ["error", "windows"] */
import ReactStars from 'react-rating-stars-component';
import React, { useState, useEffect } from 'react';
import { writeReview } from '../../requests/private-requests';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

// eslint-disable-next-line react/prop-types
const WriteReview = ({ match, history }) =>{
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const writeReviewHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();

    setValid(!valid);
    // eslint-disable-next-line react/prop-types
    form.barId = match.params.id;
    writeReview(form).then(res => {
    // eslint-disable-next-line react/prop-types
      history.push('/');
    });
  };

  return (
    <Container>
      <Form className="register-form" writeReview={writeReviewHandler} validated={valid} noValidate autoComplete="off">
        <Form.Group className="mb-4" controlId="formBasicText">
          <Form.Label>Text</Form.Label>
          <Form.Control type="text" placeholder="Enter text" required minLength={25} maxLength={1000} as='textarea' rows={5} onChange={e => setField('text', e.target.value)} />
          <Form.Text className="text-muted">
            Note: Text must be between 25-1000 characters.
          </Form.Text>
          <Form.Control.Feedback type='invalid'>
            Invalid text! Text must be between 25-1000 characters long!
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group className="mb-4" controllId="formBasicRating">
          <ReactStars
            count={5}
            onChange={(r) => form.rating = r}
            size={24}
            activeColor="#ffd700"
          />
        </Form.Group>
        <Button variant="primary" type="submit" onClick={writeReviewHandler}>
          Add review
        </Button>
      </Form>
    </Container>
  );
};

export default WriteReview;