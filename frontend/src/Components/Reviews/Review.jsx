/* eslint linebreak-style: ["error", "windows"] */
import React, { useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../context/auth-context';

// eslint-disable-next-line react/prop-types
const Review = ({ text, rating, authorName, authorId }) =>{

  const { user, isLoggedIn } = useContext(AuthContext);

  const deleteReview = (e) => {
    alert('Server does not have an implementation for removing reviews!');
  };

  return (
    <Col>
      <Container>
        <Card style={{ width: '18rem' }}>
          <Card.Body>
            <Card.Title> Author:
              {user && user.role !== '' ?
                <Link to={`/user/${authorId}`}>
                  {authorName}
                </Link> :
                <div>
                  {authorName}
                </div>}
            </Card.Title>
            <Card.Subtitle>Rating: {rating}</Card.Subtitle>
            <Card.Text>
              {text}
            </Card.Text>
            {user.id === authorId ?
              <Button onClick={(e) => deleteReview(e)}>
                Delete my review
              </Button> :
              <div> </div>}
          </Card.Body>
        </Card>
      </Container>
    </Col>
  );
};

export default Review;