/* eslint linebreak-style: ["error", "windows"] */
import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Review from './Review';
import { useState, useEffect } from 'react';
import { getReviews } from '../../requests/private-requests';

// eslint-disable-next-line react/prop-types
const ReviewsCollection = ({ match }) =>{

  const [reviews, setReviews] = useState([]);

  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    getReviews(match.params.id).then(res => setReviews(res));
  }, []);

  return (
    <Container>
      <Row>
        {reviews.map(review => {
          return (
            <Review key={review.id} text={review.text} rating={review.rating} authorName={review.author.username} authorId={review.author.id} />
          );
        })}
      </Row>
    </Container>
  );

};


export default ReviewsCollection;
