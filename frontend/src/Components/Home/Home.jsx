import React, { useContext } from 'react';
import { AuthContext } from '../../context/auth-context';
import './Home.css';
import Container from 'react-bootstrap/Container';

const Home = () => {

  const { user, isLoggedIn } = useContext(AuthContext);

  return (
    <Container fluid className="welcome-msg">
      <h1>Welcome!</h1>
      {isLoggedIn ? <h4>Hello {user.username}!</h4> : <h4>Please log in!</h4>}
    </Container>
  );
};

export default Home;
