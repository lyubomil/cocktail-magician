import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { fetchCocktailRequst } from '../../requests/server-requests';


const ShowAllCocktails = ({ history }) =>{

  const [cocktails, setCocktails] = useState([]);

  useEffect(() => {
    fetchCocktailRequst(`cocktails`)
      .then(data => {
        setCocktails(data);
      })
      .catch(e => {
        console.log(e, `Error: ${e}`);
      });
  }, []);


  const routeChange = (id) =>{
    const path = `/cocktails/${id}`;
    history.push(path);
  };

  return (
    <Container>
      <Row xs={1} md={3} className="g-4">
        {cocktails.map(cocktail => {
          return (
            <Col key={cocktail.id}>
              <Container>
                <Card style={{ width: '20rem' }}>
                  <Card.Img variant="top" src={`http://localhost:5000${cocktail.imageUrl}`} />
                  <Card.Body>
                    <Card.Title>{cocktail.name}</Card.Title>
                    <Card.Subtitle>Ingredients: </Card.Subtitle>
                    {cocktail.ingredients.map(ing => {
                      return (
                        <Card.Text key={ing.id}>
                          * {ing.name}
                        </Card.Text>
                      );
                    })}
                    <Button onClick={() => routeChange(cocktail.id)}>
                      Details
                    </Button>
                  </Card.Body>
                </Card>
              </Container>
            </Col>
          );
        })}
      </Row>
    </Container>
  );

};

ShowAllCocktails.propTypes = {
  history: PropTypes.object,
};


export default ShowAllCocktails;
