import React from 'react';
import Container from 'react-bootstrap/Container';
import { useState, useEffect, useContext } from 'react';
import { fetchCocktailRequst } from '../../requests/server-requests';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { AuthContext } from '../../context/auth-context';


// eslint-disable-next-line react/prop-types
const CocktailDetails = ({ match, history }) => {
  const [cocktail, setCocktail] = useState({ name: '', imageUrl: '', ingredients: [] });
  const { user, isLoggedIn } = useContext(AuthContext);

  // TODO add availableIn, favouriteCount, favouritedBy to details showage
  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    fetchCocktailRequst(`cocktails/${match.params.id}`)
      .then(cocktailResult => {
        setCocktail(cocktailResult);
      })
      .catch(e => {
        console.log(e);
      });
  }, {});

  const adminHandler = () => {
    if (user?.role === 'magician') { // TODO Check for admin permissions
      return (
      // eslint-disable-next-line react/prop-types
        <Button onClick={() => history.push(`/cocktails/edit/${cocktail.id}`)}>
          Edit
        </Button>
      );
    } else {
      return '';
    };
  };

  return (
    <Container className="centeredCont">
      <Card style={{ width: '50%' }}>
        <Card.Body>
          <Card.Img variant="top" src={`http://localhost:5000${cocktail.imageUrl}`} />
          <Card.Title>{cocktail.name}</Card.Title>
          <Card.Subtitle>Ingredients: </Card.Subtitle>
          {cocktail.ingredients.map(ing => {
            return (
              <Card.Text key={ing.id}>
                * {ing.name}
              </Card.Text>
            );
          })}
          {adminHandler()}
        </Card.Body>
      </Card>
    </Container>
  );
};

export default CocktailDetails;
