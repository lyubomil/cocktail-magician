/* eslint linebreak-style: ["error", "windows"] */
import React, { useContext, useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';
import { getUser } from '../../requests/private-requests';
import Button from 'react-bootstrap/Button';
import { AuthContext } from '../../context/auth-context';
import Review from '../Reviews/Review';

// eslint-disable-next-line react/prop-types
const UserDeatils = ({ match, history }) =>{
  const { user, isLoggedIn } = useContext(AuthContext);
  const [userDetails, setUserDetails] = useState({ name: '', displayName: '' });
  const [reviewedBars, setReviewedBars] = useState([]);

  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    getUser(match.params.id).then(res => {
      setUserDetails(res);
      setReviewedBars(res.reviewedBars);
    });
  }, []);

  const changeDisplayName = () => {
    const path = `/user-changeDisplayName/${user.id}`;
    // eslint-disable-next-line react/prop-types
    history.push(path);
  };

  return (
    <Container>
      <Card style={{ width: '80%' }}>
        <Card.Body>
          <Card.Title>Username: {userDetails.username}</Card.Title>
          <Card.Subtitle>Display name: {userDetails.displayName ? userDetails.displayName : 'No dispaly name!'}</Card.Subtitle>
          {user.id === userDetails.id ?
            <Button onClick={() => changeDisplayName()}>
              Change Display name
            </Button> :
            <div> </div>}
          <br />
          {reviewedBars.map(bar => {
            console.log(bar);
            return (
              <div style={{ float: 'left' }} key={bar.id}>
                <Card.Title>Bar name: {bar.name}</Card.Title>
                <Card.Subtitle>Your rating: {bar.myRating}</Card.Subtitle>
                <Review key={bar.id} text={bar.text} rating={bar.avgRating} authorName={userDetails.username} authorId={userDetails.id} />
              </div>
            );
          })}
        </Card.Body>
      </Card>
    </Container>
  );
};

export default UserDeatils;