/* eslint linebreak-style: ["error", "windows"] */
import React, { useState, useContext } from 'react';
import { changeDisplayName } from '../../requests/private-requests';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { AuthContext } from '../../context/auth-context';

// eslint-disable-next-line react/prop-types
const ChangeUserDisplayName = ({ match, history }) =>{
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const changeDisplayNameHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();

    setValid(!valid);
    // eslint-disable-next-line react/prop-types
    changeDisplayName(form).then(res => {
      // eslint-disable-next-line react/prop-types
      history.push(`/user/${match.params.id}`);
    });
  };

  return (
    <Container>
      <Form className="register-form" changeDisplayName={changeDisplayNameHandler} validated={valid} noValidate autoComplete="off">
        <Form.Group className="mb-4" controlId="formBasicDisplayName">
          <Form.Label>Display Name</Form.Label>
          <Form.Control type="text" placeholder="Enter dispaly name" required minLength={3} maxLength={30} onChange={e => setField('displayName', e.target.value)} />
          <Form.Text className="text-muted">
            Note: Display name must be between 3-30 characters.
          </Form.Text>
          <Form.Control.Feedback type='invalid'>
            Invalid text! Display name must be between 3-30 characters!
          </Form.Control.Feedback>
        </Form.Group>
        <Button variant="primary" type="submit" onClick={changeDisplayNameHandler}>
          Change display name
        </Button>
      </Form>
    </Container>
  );
};

export default ChangeUserDisplayName;