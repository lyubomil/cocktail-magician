/* eslint linebreak-style: ["error", "windows"] */
import React, { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { editBar } from '../../requests/admin-requests';
import { fetchBarRequest } from '../../requests/server-requests';

// eslint-disable-next-line react/prop-types
const EditBar = ({ match, history }) => {
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});
  const [bar, setBar] = useState({});

  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    fetchBarRequest(`bars/${match.params.id}`)
      .then(barResult => {
        setBar(barResult);
      })
      .catch(e => {
        console.log(e);
      });
  }, []);

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const editBarHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);

    form.id = bar.id;
    editBar(form).then(res => {
      // eslint-disable-next-line react/prop-types
      history.push('/bars');
    });
  };

  return (
    <Form className="register-form" editBarHandler={editBarHandler} validated={valid} noValidate autoComplete="off">
      <Form.Group className="mb-3" controlId="formBasicUsername">
        <Form.Label>Name</Form.Label>
        <Form.Control type="text" placeholder={bar.name} required minLength={5} maxLength={50} onChange={e => setField('name', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Name of the bar must be between 5-50 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid name! Name must be between 5-50 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPhone">
        <Form.Label>Phone</Form.Label>
        <Form.Control type="phone" required minLength={2} maxLength={20} placeholder={bar.phone} onChange={e => setField('phone', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Phone must be between 2-20 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid phone! Phone must be between 2-20 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicAddress">
        <Form.Label>Address</Form.Label>
        <Form.Control type="address" required minLength={5} maxLength={100} placeholder={bar.address} onChange={e => setField('address', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Address must be between 5-100 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid address! Address must be between 5-100 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Button variant="dark" type="submit" onClick={editBarHandler}>
        Edit Bar
      </Button>
    </Form>
  );
};


export default EditBar;
