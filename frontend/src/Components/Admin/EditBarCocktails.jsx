/* eslint linebreak-style: ["error", "windows"] */
/* eslint-disable */
import React, { useEffect, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { getBar, getCocktails } from '../../requests/private-requests';
import { editBarCollection } from '../../requests/admin-requests';

// eslint-disable-next-line react/prop-types
const EditBar = ({ match, history }) => {
  const [bar, setBar] = useState({});
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});
  const [curCocktails, setCurCocktails] = useState([]);
  const [newCocktails, setNewCocktails] = useState([]);

  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    form.curCocktails = [];
    form.newCocktails = [];
    form.cocktailsToRemove = [];
    getBar(match.params.id).then(res => {
      setBar(res);
      setCurCocktails(res.cocktails);
      res.cocktails.map(c => {
        form.curCocktails.push(c.id);
      })
      console.log(bar);
    });
    getCocktails().then(res => setNewCocktails(res));
    console.log(newCocktails);
  }, []);

  const getCurrentCocktails = (e) => {
    const curCocktails = bar.cocktails;
    if (curCocktails !== undefined) {
      return (
        curCocktails.map(c => {
          return (
            form.curCocktails.includes(c.id) ?
              <div key={c.id}>
                <Form.Label> {c.name} </Form.Label>
                <Button
                  value={c.id} variant="dark" type="submit" onClick={(e) => {
                    e.preventDefault();
                    const index = form.curCocktails.indexOf(c.id);
                    if (index !== -1) {
                      form.curCocktails.splice(index, 1);
                    }
                    form.cocktailsToRemove.push(c.id);
                    setNewCocktails([...newCocktails]);
                  }}>
                  -
                </Button>
              </div> :
              <div key={c.id}>
                <Form.Label> {c.name} </Form.Label>
                <Button value={c.id} variant="dark" type="submit" onClick={(e) => {
                  e.preventDefault();
                  const index = form.cocktailsToRemove.indexOf(c.id);
                  if (index !== -1) {
                    form.ingToRemoveIngredients.splice(index, 1);
                  }
                  form.curCocktails.push(c.id);
                  setNewCocktails([...newCocktails]);
                }}>
                  +
                </Button>
              </div>
          );
        })
      );
    } else {
      return 0;
    }
  };

  const getNewCocktails = (e) => {
    const curCoktails = bar.cocktails;
    if (curCoktails !== undefined) {
      const otherCocktails = newCocktails.filter(c => {
        const has = curCoktails.some(ci => ci.name === c.name);
        if (!has) {
          return c;
        } else {
          return 0;
        }
      });
      return (
        otherCocktails.map(c => {
          return (
            form.newCocktails.includes(c.id) ?
              <div key={c.id}>
                <Form.Label> {c.name} </Form.Label>
                <Button
                  value={c.id} variant="dark" type="submit" onClick={(e) => {
                    e.preventDefault();
                    const index = form.newCocktails.indexOf(c.id);
                    if (index !== -1) {
                      form.newCocktails.splice(index, 1);
                    }
                    console.log(form.newCocktails);
                    setNewCocktails([...newCocktails]);
                  }}>
                  -
                </Button>
              </div> :
              <div key={c.id}>
                <Form.Label> {c.name} </Form.Label>
                <Button value={c.id} variant="dark" type="submit" onClick={(ev) => {
                  ev.preventDefault();
                  form.newCocktails.push(c.id);
                  setNewCocktails([...newCocktails]);
                }}>
                  +
                </Button>
              </div>
          );
        })
      );
    } else {
      return 0;
    }
  };

  const editCocktailHandler = (e) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);
    form.id = bar.id;
    editBarCollection(form).then(res => {
    // eslint-disable-next-line react/prop-types
      history.push('/');
    });
  };

  return (
    <Container className="centeredCont">
      <Card style={{ width: '50%' }}>
        <Card.Img variant="top" src={typeof (bar.images) === 'undefined' ? 'f' : `http://localhost:5000${bar.images[0].url}`} />
        <Card.Body>
          <Card.Title>{bar.name}</Card.Title>
          <Card.Body>Address: {bar.address}<br />Phone: {bar.phone}</Card.Body>
        </Card.Body>
      </Card>
      <Row>
        <Form className="register-form" EditCocktail={editCocktailHandler} validated={valid} noValidate autoComplete="off">
          <Form.Group>
            <Form.Label>Current Cocktails</Form.Label>
            <Col>{getCurrentCocktails()}</Col>
          </Form.Group>
          <br />
          <Form.Group>
            <Form.Label>Other Cocktails</Form.Label>
            <Col>{getNewCocktails()}</Col>
          </Form.Group>
          <Button variant="dark" type="submit" onClick={editCocktailHandler}>
            Edit cocktail collection
          </Button>
        </Form>
      </Row>
    </Container>
  );
};


export default EditBar;
