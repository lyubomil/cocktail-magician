import { createIngredient } from '../../../requests/admin-requests';
import Container from 'react-bootstrap/Container';
import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

const CreateIngredient = ( match ) =>{
  const [form, setForm] = useState({});

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const createIngredientHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();

    createIngredient(form)
      .then(match.history.push('/'));
  };

  return (
    <Container>
      <Form createIngredient={createIngredientHandler}>
        <Form.Control type="text" placeholder="Normal text" onChange={e => setField('name', e.target.value)} />
        <Button variant="primary" type="submit" onClick={createIngredientHandler}>
          Create
        </Button>
      </Form>
    </Container>
  );
};

export default CreateIngredient;