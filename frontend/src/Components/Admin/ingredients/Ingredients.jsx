import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect } from 'react';
import { fetchRequest } from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';
import { deleteIngredient } from '../../../requests/admin-requests';

const Ingredients = () =>{

  const [ingredients, setIngredients] = useState([]);

  useEffect(() => {
    const args = {
      path: `ingredients`,
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: response => setIngredients(response)
    };

    fetchRequest(args);
    console.log(ingredients);
  }, []);

  const deleteIng = (id) =>{
    const path = `/ingredients/${id}`;
    deleteIngredient(id).then(res => {
      setIngredients(ingredients.filter(i => i.id !== id));
    });
  };

  return (
    <Container>
      <Row>
        {ingredients.filter(i => i.cocktailsCount > 0).map(ingredient =>{
          return (
            <Col key={ingredient.id}>
              <Container>
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>{ingredient.name}</Card.Title>
                  </Card.Body>
                </Card>
              </Container>
            </Col>
          );
        })}
      </Row>
      <br />
      <Row>
        {ingredients.filter(i => i.cocktailsCount === 0).map(ingredient =>{
          return (
            <Col key={ingredient.id}>
              <Container>
                <Card style={{ width: '18rem' }}>
                  <Card.Body>
                    <Card.Title>{ingredient.name}</Card.Title>
                    <Button onClick={() => deleteIng(ingredient.id)}>
                      Delete
                    </Button>
                  </Card.Body>
                </Card>
              </Container>
            </Col>
          );
        })}
      </Row>
    </Container>
  );

};


export default Ingredients;
