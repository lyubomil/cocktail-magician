import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { fetchRequest } from '../../requests/server-requests';
import { getToken } from '../../context/auth-context';

const CreateBar = () => {
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const createBar = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);

    const formData = new FormData;

    Object.keys(form).forEach(key => formData.append(key, form[key]));

    const args = {
      path: 'bars',
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: (response)=> {
        console.log(response);
      } };

    fetchRequest(args);
  };

  return (
    <Form className="register-form" createBar={createBar} validated={valid} noValidate autoComplete="off">
      <Form.Group className="mb-3" controlId="formBasicUsername">
        <Form.Label>Name</Form.Label>
        <Form.Control type="text" placeholder="Enter name" required minLength={5} maxLength={50} onChange={e => setField('name', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Name of the bar must be between 5-50 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid name! Name must be between 5-50 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPhone">
        <Form.Label>Phone</Form.Label>
        <Form.Control type="phone" required minLength={2} maxLength={20} placeholder="Phone" onChange={e => setField('phone', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Phone must be between 2-20 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid phone! Phone must be between 2-20 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicAddress">
        <Form.Label>Address</Form.Label>
        <Form.Control type="address" required minLength={5} maxLength={100} placeholder="Address" onChange={e => setField('address', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Address must be between 5-100 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid address! Address must be between 5-100 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="formFileMultiple" className="mb-3">
        <Form.Label>Upload Images</Form.Label>
        <Form.Control
          type="file"
          onChange={e => setField('images', e.target.files[0])}
        />
      </Form.Group>

      <Button variant="dark" type="submit" onClick={createBar}>
        Create Bar
      </Button>
    </Form>
  );
};


export default CreateBar;
