/* eslint linebreak-style: ["error", "windows"]*/
/* eslint-disable */
import ImageUploader from 'react-images-upload';
import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { getIngredients, editCocktail } from '../../../requests/admin-requests';
import { getCocktail } from '../../../requests/private-requests';

// eslint-disable-next-line react/prop-types
const EditCocktail = ({ match, history }) => {
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});
  const [ingredients, setIngredients] = useState([]);
  const [cocktail, setCocktail] = useState({});

  useEffect(() => {
    form.ingredients = [];
    form.curIngredients = [];
    form.ingToRemoveIngredients = [];
    getIngredients().then(data => setIngredients(data));
    // eslint-disable-next-line react/prop-types
    getCocktail(match.params.id).then(data => {
      data.ingredients.map(i => {
        form.curIngredients.push(i.name);
      });
      setCocktail(data);
    });
  }, []);

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const addIngredientToCocktail = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const ingredient = e.target.value;
    if (!form.ingredients) {
      form.ingredients = [ingredient];
    } else {
      form.ingredients = [...form.ingredients, ingredient];
    }
    setIngredients([...ingredients]);
  };

  const removeIngredientToCocktail = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const item = e.target.value;
    const index = form.ingredients.indexOf(item);
    if (index !== -1) {
      form.ingredients.splice(index, 1);
    }
    setIngredients([...ingredients]);
  };

  const editCocktailHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);
    form.id = cocktail.id;
    editCocktail(form).then(res => {
      history.push('/');
    });
  };

  const getCurrentIngredients = (e) => {
    const currIngredients = cocktail.ingredients;
    if (currIngredients !== undefined) {
      return (
        currIngredients.map(i => {
          return (
            form.curIngredients.includes(i.name) ?
              <div key={i.id}>
                <Form.Label> {i.name} </Form.Label>
                <Button
                  value={i.name} variant="dark" type="submit" onClick={(e) => {
                    e.preventDefault();
                    const index = form.curIngredients.indexOf(i.name);
                    if (index !== -1) {
                      form.curIngredients.splice(index, 1);
                    }
                    form.ingToRemoveIngredients.push(i.name);
                    setIngredients([...ingredients]);
                  }}>
                  -
                </Button>
              </div> :
              <div key={i.id}>
                <Form.Label> {i.name} </Form.Label>
                <Button value={i.name} variant="dark" type="submit" onClick={(e) => {
                    e.preventDefault();
                    const index = form.ingToRemoveIngredients.indexOf(i.name);
                    if (index !== -1) {
                      form.ingToRemoveIngredients.splice(index, 1);
                    }
                    form.curIngredients.push(i.name);
                    setIngredients([...ingredients]);
                  }}>
                  +
                </Button>
              </div>
          );
        })
      );
    } else {
      return 0;
    }
  };

  const getOtherIngredients = (e) => {
    const currIngredients = cocktail.ingredients;
    if (currIngredients !== undefined) {
      const otherIngredients = ingredients.filter(i => {
        const has = currIngredients.some(ci => ci.name === i.name);
        if (!has) {
          return i;
        } else {
          return 0;
        }
      });
      return (
        otherIngredients.map(i => {
          return (
            form.ingredients.includes(i.name) ?
              <div key={i.id}>
                <Form.Label> {i.name} </Form.Label>
                <Button value={i.name} variant="dark" type="submit" onClick={removeIngredientToCocktail}>
                  -
                </Button>
              </div> :
              <div key={i.id}>
                <Form.Label> {i.name} </Form.Label>
                <Button value={i.name} variant="dark" type="submit" onClick={addIngredientToCocktail}>
                  +
                </Button>
              </div>
          );
        })
      );
    } else {
      return 0;
    }
  };

  return (
    <Form className="register-form" EditCocktail={editCocktailHandler} validated={valid} noValidate autoComplete="off">
      <Form.Group className="mb-3" controlId="formBasicname">
        <Form.Label>Name</Form.Label>
        <Form.Control type="text" placeholder={cocktail.name} required minLength={2} maxLength={50} onChange={e => setField('name', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Name of the cocktail must be between 2-50 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid name! Name must be between 2-50 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="formFile" className="mb-3">
        <Form.Label>Upload Images</Form.Label>
        <ImageUploader
          withIcon={1}
          buttonText='Choose images'
          onChange={e => setField('image', e[0])}
          imgExtension={['.jpg', '.gif', '.png', '.gif']}
          maxFileSize={5242880}
        />
      </Form.Group>

      <Form.Group>
        <Form.Label>Current Ingredients</Form.Label>
        {getCurrentIngredients()}
      </Form.Group>
      <br />
      <Form.Group>
        <Form.Label>Other Ingredients</Form.Label>
        {getOtherIngredients()}
      </Form.Group>

      <Button variant="dark" type="submit" onClick={editCocktailHandler}>
        Edit cocktail
      </Button>
    </Form>
  );
};
export default EditCocktail;