/* eslint linebreak-style: ["error", "windows"]*/
import ImageUploader from 'react-images-upload';
import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { getIngredients } from '../../../requests/admin-requests';
import { fetchRequest } from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';

const CreateCocktail = (props) => {
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});
  const [ingredients, setIngredients] = useState([]);

  useEffect(() => {
    getIngredients().then(data => setIngredients(data));
    form.ingredients = [];
  }, []);

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const addIngredientToCocktail = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const ingredient = e.target.value;
    if (!form.ingredients) {
      form.ingredients = [ingredient];
    } else {
      form.ingredients = [...form.ingredients, ingredient];
    }
    setIngredients([...ingredients]);
  };

  const removeIngredientToCocktail = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const item = e.target.value;
    const index = form.ingredients.indexOf(item);
    if (index !== -1) {
      form.ingredients.splice(index, 1);
    }
    setIngredients([...ingredients]);
  };

  const createCocktail = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);

    const formData = new FormData;
    formData.append('name', form['name']);
    formData.append('image', form['image']);
    const ingredientsArr = form['ingredients'];
    for (let i = 0; i < ingredientsArr.length; i++) {
      formData.append('ingredients[]', ingredientsArr[i]);
    }

    const args = {
      path: 'cocktails',
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: (response) => {
        console.log(response);
      }
    };

    fetchRequest(args).then(() => {
      window.location.href = '/';
    });

  };

  return (
    <Form className="register-form" CreateCocktail={createCocktail} validated={valid} noValidate autoComplete="off">
      <Form.Group className="mb-3" controlId="formBasicname">
        <Form.Label>Name</Form.Label>
        <Form.Control type="text" placeholder="Enter name" required minLength={2} maxLength={50} onChange={e => setField('name', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Name of the cocktail must be between 2-50 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid name! Name must be between 2-50 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="formFile" className="mb-3">
        <Form.Label>Upload Images</Form.Label>
        <ImageUploader
          withIcon={1}
          buttonText='Choose images'
          onChange={e => setField('image', e[0])}
          imgExtension={['.jpg', '.gif', '.png', '.gif']}
          maxFileSize={5242880}
        />
      </Form.Group>

      <Form.Group>
        {ingredients.map(i => {
          return (
            form.ingredients.includes(i.name) ?
              <div key={i.id}>
                <Form.Label> {i.name} </Form.Label>
                <Button value={i.name} variant="dark" type="submit" onClick={removeIngredientToCocktail}>
                  -
                </Button>
              </div> :
              <div key={i.id}>
                <Form.Label> {i.name} </Form.Label>
                <Button value={i.name} variant="dark" type="submit" onClick={addIngredientToCocktail}>
                  +
                </Button>
              </div>
          );
        })}
      </Form.Group>

      <Button variant="dark" type="submit" onClick={createCocktail}>
        Create cocktail
      </Button>
    </Form>
  );
};
export default CreateCocktail;