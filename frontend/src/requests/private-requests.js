/* eslint-disable */
import { getToken } from '../context/auth-context';

export const getBars = () => {
    return fetch(`http://localhost:5000/bars`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
}

export const getCocktails = () => {
    return fetch(`http://localhost:5000/cocktails`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
}

export const getCocktail = (cocktailId) => {
    return fetch(`http://localhost:5000/cocktails/${cocktailId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
};

export const getBar = (barId) => {
    return fetch(`http://localhost:5000/bars/${barId}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
};

export const getReviews = (barId) => {
    return fetch(`http://localhost:5000/bars/${barId}/reviews`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
}

export const writeReview = (data) => {
    const review = {
        'text': data.text,
        'rating': data.rating,
    };
    return fetch(`http://localhost:5000/bars/${data.barId}/reviews`, {
        method: 'POST',
        body: JSON.stringify(review),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
}

export const getUser = (id) => {
    return fetch(`http://localhost:5000/users/${id}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
}

export const changeDisplayName = (user) => {
    return fetch(`http://localhost:5000/users/`, {
        method: 'PUT',
        body: JSON.stringify(user),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
}

export const getBarsByName = (barName) => {
    return fetch(`http://localhost:5000/bars/?name=${barName}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
}

export const getBarsByIngredient = (ingredientName) => {
    return fetch(`http://localhost:5000/bars/cocktail-ingredients?values=${ingredientName}`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
}