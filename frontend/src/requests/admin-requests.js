/* eslint-disable */
import { getToken } from '../context/auth-context';

export const getIngredients = () => {
    return fetch(`http://localhost:5000/ingredients`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
};

export const deleteIngredient = (ingredientId) => {
    return fetch(`http://localhost:5000/ingredients/${ingredientId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
};

export const createIngredient = (ingredient) => {
    return fetch(`http://localhost:5000/ingredients`, {
        method: 'POST',
        body: JSON.stringify(ingredient),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
};

export const editBar = (bar) => {
    return fetch(`http://localhost:5000/bars/${bar.id}`, {
        method: 'PUT',
        body: JSON.stringify(bar),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
};

export const editCocktail = (cocktail) => {
    const formData = new FormData;
    formData.append('name', cocktail.name);
    formData.append('image', cocktail.image);
    formData.append('id', cocktail.id);

    const ing = {
        'id': cocktail.id,
        'ingredients': [...cocktail.ingredients, ...cocktail.curIngredients],
    };

    fetch(`http://localhost:5000/cocktails/${cocktail.id}/ingredients`, {
        method: 'PUT',
        body: JSON.stringify(ing),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
    
    if (cocktail.ingToRemoveIngredients.length > 0) {
        const ingToRemove = {
            'id': cocktail.id,
            'ingredients': [...cocktail.ingToRemoveIngredients],
        };

        fetch(`http://localhost:5000/cocktails/${cocktail.id}/ingredients`, {
        method: 'DELETE',
        body: JSON.stringify(ingToRemove),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
        },
        }).then(data => data.json())
            .catch(e => console.log(e.message));
    }

    return fetch(`http://localhost:5000/cocktails/${cocktail.id}`, {
        method: 'PUT',
        body: formData,
        headers: {
            'Authorization': `Bearer ${getToken()}`,
        },
    }).then(data => data.json())
        .catch(e => console.log(e.message));
}

export const editBarCollection = (bar) => {
    const formData = new FormData;
    formData.append('id', bar.id);

    bar.newCocktails.map(c => {
        const cocktail = { 'barId': bar.id, 'cocktailId': c};
        fetch(`http://localhost:5000/bars/${bar.id}/cocktails/${c}`, {
            method: 'PUT',
            body: JSON.stringify(cocktail),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`,
            },
        }).then(data => data.json())
            .catch(e => console.log(e.message));
    });
    
    if (bar.cocktailsToRemove.length > 0) {
        bar.cocktailsToRemove.map(c => {
            const cocktail = { 'barId': bar.id, 'cocktailId': c};
            fetch(`http://localhost:5000/bars/${bar.id}/cocktails/${c}`, {
                method: 'DELETE',
                body: JSON.stringify(cocktail),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${getToken()}`,
                },
            }).then(data => data.json())
                .catch(e => console.log(e.message));
        });
    }

    return Promise.resolve('Success');
}