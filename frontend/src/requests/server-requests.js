import { getToken } from '../context/auth-context';

export const fetchRequest = async ({ path, method, body, handler, errorHandler, headers }) => {
  let result = null;
  try {
    const response = await fetch(`http://localhost:5000/${path}`, {
      method: method || 'GET',
      body,
      headers: headers,
    });
    const dataJSON = await response.json();
    result = await handler(dataJSON);
  } catch (err) {
    console.error(err);
    errorHandler && errorHandler(err);
  }

  return result;
};

export const fetchBarRequest = (path) => {
  return fetch(`http://localhost:5000/${path}`, {
    method: 'GET',
    body: null,
    headers: {
      'Authorization': `Bearer ${getToken()}`
    }
  })
    .then(response => {
      return response.ok ? response.json() :
        response.text().then(text => {
          throw text;
        });
    });

};

export const fetchCocktailRequst = (path) => {
  return fetch(`http://localhost:5000/${path}`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${getToken()}`
    }
  })
    .then(response => {
      return response.ok ? response.json() :
        response.text().then(text => {
          throw text;
        });
    });

};