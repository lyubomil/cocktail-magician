/* eslint-disable require-jsdoc */
import './App.css';
import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavigationBar from './Navigation/Navbar.jsx';
import { BrowserRouter, Switch, Route, Redirect, useRouteMatch } from 'react-router-dom';
import Home from './Components/Home/Home';
import Register from './Components/Register/Register';
import LogIn from './Components/LogIn/LogIn';
import { AuthContext, getUser, getDecodedUser } from './context/auth-context';
import PropTypes from 'prop-types';

// Users
import UserDeatils from './Components/Users/UserDetails';
import ChangeUserDisplayName from './Components/Users/ChangeUserDisplayName';

// Reviews
import WriteReview from './Components/Reviews/WriteReview';
import ReviewsCollection from './Components/Reviews/ReviewsCollection';

// Cocktails
import CreateCocktail from './Components/Admin/Cocktail/CreateCocktail';
import EditCocktail from './Components/Admin/Cocktail/EditCocktail';

// Ingredients
import CreateIngredient from './Components/Admin/Ingredients/CreateIngredient';
import Ingredients from './Components/Admin/Ingredients/Ingredients';

// Bars
import ShowAllBars from './Components/Bars/ShowAllBars';
import BarDetails from './Components/Bars/BarDetails';
import CreateBar from './Components/Admin/CreateBar';
import EditBar from './Components/Admin/EditBar';
import EditBarCocktails from './Components/Admin/EditBarCocktails';
import ShowAllCocktails from './Components/Cocktails/ShowAllCocktails';
import CocktailDetails from './Components/Cocktails/CocktailDetails';

const PrivateRoute = ({ component: Component, auth, ...rest }) => {
  return (
    <Route
      {...rest} render={(props) => {
        return auth ?
          <Component {...props} /> :
          <Redirect to='/login' />;
      }}
    />
  );
};

// eslint-disable-next-line func-style
function App() {
  const user = getDecodedUser();
  const [auth, setAuth] = useState({
    user,
    isLoggedIn: !!user
  });

  useEffect(() => {
    getUser()
      .then(setAuth)
      .catch(err => console.error(err));
  }, []);

  return (
    <AuthContext.Provider value={{ ...auth, setAuth }}>
      <BrowserRouter>
        <NavigationBar />
        <Switch>
          <Route path="/home" component={Home} />
          <Route exact path="/bars" component={ShowAllBars} />
          <Route exact path="/cocktails" component={ShowAllCocktails} />
          <Route exact path="/ingredients" component={Ingredients} />
          <Route path="/register" component={Register} />
          <Route exact path="/">
            <Redirect to={auth?.isLoggedIn ? '/home' : '/login'} />
          </Route>
          <Route path="/login" component={LogIn} />
          <Route path="/user/:id" component={UserDeatils} />
          <Route path="/user-changeDisplayName/:id" component={ChangeUserDisplayName} />
          <Route exact path="/bars/:id" component={BarDetails} />
          <Route exact path="/cocktails/:id" component={CocktailDetails} />
          <Route path="/create-bar" component={CreateBar} />
          <Route path="/bars/edit/:id" component={EditBar} />
          <Route path="/bars/add-review/:id" component={WriteReview} />
          <Route path="/bars/reviews/:id" component={ReviewsCollection} />
          <Route path="/bars/editCocktails/:id" component={EditBarCocktails} />
          <Route path="/cocktails/edit/:id" component={EditCocktail} />
          <Route path="/create-ingredient" component={CreateIngredient} />
          <Route path="/create-cocktail" component={CreateCocktail} />
        </Switch>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

PrivateRoute.propTypes = {
  component: PropTypes.func,
  auth: PropTypes.bool
};
export default App;
